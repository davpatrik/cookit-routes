# Project description

## "cookit-routes"
It is a spring boot microservice project (SpringBoot v2.4.1 + Maven POM 4.0.0 + JWT + JUnit), which provides REST service to process items and obtain routes.

## CI/CD configurations
cookit-routes contains file [.gitlab-ci.yml] which is the pipeline configuration file for GitLab, it contains the following stages:
- build
- test: Run tests for ensure code quality
- package: Build and push docker image into [DockerHub repository](https://hub.docker.com/repository/docker/davpatrik/cookit-routes)
- deploy: Deploy the image form [DockerHub repository](https://hub.docker.com/repository/docker/davpatrik/cookit-routes) into a Kubernetes Cluster configured from scratch in DigitalOcean Cloud Services. It depends of file '/kubernetes/deployment.yml'

## Software requirements
It is required a previous installation of:
- openjdk 11: [Instalación guide](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/).
- Git: [Instalación guide](https://github.com/git-guides/install-git#:~:text=To%20install%20Git%2C%20navigate%20to,installation%20by%20typing%3A%20git%20version%20.).
- Maven: [Instalación guide](https://www.mkyong.com/maven/how-to-install-maven-in-windows/).
- Docker: [Instalación guide](https://runnable.com/docker/install-docker-on-linux).

# Project download and execution

## Docker image pull
In order to download the docker image, execute the following command:

```bash
docker pull davpatrik/cookit-routes:latest
```

## Docker image run
In order to run the docker image, execute the following command:

```bash
docker run -p 8081:8081 davpatrik/cookit-routes:latest
```

## Download cookit-routes from Git repository
Download sources at GitHub [https://gitlab.com/davpatrik/cookit-routes.git](https://gitlab.com/davpatrik/cookit-routes.git). 

## Running cookit-routes
Run project in IDE: 
-In Eclipse: selectProject->RunAs->3 Spring Boot App, or 
-In NetBeans: selectProject->buildWhitDependencies then selectProject->Run 
Or in command line, in CMD first go to main project folder "..\cookit-routes" and execute the following command:
```bash
mvn spring-boot:run
```
# Project testing

## Once obtained a jwtKey ($token) from "cookit-login" microservice, use it plus the following apiKey to consume principal REST service from "cookit-routes" project
Try POST in the next URL in postman-> http://localhost:8081/routes, send the next params

### Header params
```bash
Content-Type: application/json
API-KEY: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c
JWT-KEY: $token
```
### Body params
```bash
{
    "itemIds": [1, 2, 3, 4, 123, 125, 130, 9999]
}
```

Or simply try CURL
```bash
curl -X POST \
-H "API-KEY: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c" \
-H "JWT-KEY: $token" \
-H "Content-Type: application/json" \
-d '{ "itemIds": [1, 2, 3, 4, 123, 125, 130, 9999] }' \
http://localhost:8081/routes
```

The result should be:
```bash
{
    "picks": [
        "A1",
        "C2",
        "B3"
    ],
    "outOfStock": [
        "A3"
    ]
}
```
## //TODO
- Add SONAR dependencies for static code revision.
- Enable Kubernetes Cluster on GitLab configuration in order to enable CD (continuous delivery).