package com.cookit.test.controller;

import com.cookit.dto.ItemDto;
import com.cookit.dto.ProteinsDto;
import com.cookit.service.impl.ItemsServiceImpl;
import com.util.AppUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ItemsServiceImpl productService;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST ProductService contextLoads");
        org.assertj.core.api.Assertions.assertThat(productService).isNotNull();
    }

    @Test
    public void whenRequestForItems_thenReturnCorrectResponse() {
        System.out.println("TEST ProductService.whenRequestForProducts_thenReturnCorrectResponse");
        try {
            List<ItemDto> lstQueried = productService.queryLstItems();
            System.out.println("lstQueried: " + (lstQueried == null ? 0 : lstQueried.size()));
            Assert.assertTrue(lstQueried != null && !lstQueried.isEmpty());
        } catch (Exception ex) {
            Logger.getLogger(ProductServiceTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.assertTrue(AppUtil.getStackTraceAsString(ex), false);
        }
    }

    @Test
    public void whenRequestForProteins_thenReturnCorrectResponse() {
        System.out.println("TEST ProductService.whenRequestForProteins_thenReturnCorrectResponse");
        try {
            List<ProteinsDto> lstQueried = productService.queryLstProteins();
            System.out.println("lstQueried: " + (lstQueried == null ? 0 : lstQueried.size()));
            Assert.assertTrue(lstQueried != null && !lstQueried.isEmpty());
        } catch (Exception ex) {
            Logger.getLogger(ProductServiceTest.class.getName()).log(Level.SEVERE, null, ex);
            Assert.assertTrue(AppUtil.getStackTraceAsString(ex), false);
        }
    }
    
}
