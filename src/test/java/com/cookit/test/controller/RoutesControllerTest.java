package com.cookit.test.controller;

import com.cookit.controller.RoutesController;
import com.cookit.dto.RoutesPayloadDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoutesControllerTest {

    @Autowired
    private RoutesController routesController;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST RoutesController contextLoads");
        org.assertj.core.api.Assertions.assertThat(routesController).isNotNull();
    }

    @Test
    public void whenBadRequest_thenReturnBadResponse() {
        System.out.println("TEST RoutesController.whenBadRequest_thenReturnBadResponse");
        try {
            RoutesPayloadDto payload = null;            
            ResponseEntity response = routesController.queryRoutesByItems(payload);
            System.out.println("response: " + response);
            Assert.assertTrue(response.getStatusCode().equals(HttpStatus.BAD_REQUEST));            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
