package com.cookit.test.controller;

import com.cookit.controller.ItemsController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemsControllerTest {

    @Autowired
    private ItemsController itemsController;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST ItemsController contextLoads");
        org.assertj.core.api.Assertions.assertThat(itemsController).isNotNull();
    }

}
