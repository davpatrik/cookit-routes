package com.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author davpatrik
 */
public class AppUtil {

    public static String getStackTraceAsString(final Throwable throwable) {
        try {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw, true);
            throwable.printStackTrace(pw);            
            return throwable.getMessage() + ", detail: " + sw.getBuffer().toString();
        } catch (Exception ex) {
            return "EX (getStackTraceAsString)";
        }
    }

    public static String jsonObtenerStrDesdeObjeto(Object obj) {
        try {
            if (obj == null) {
                return "(obtenerStrDesdeObjeto) null";
            } else {                
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                return gson.toJson(obj);
            }
        } catch (Exception ex) {
            return "(obtenerStrDesdeObjeto) " + ex.toString(); //MegaUtilExceptionHandler.getStackTraceAsString(ex);
        }
    }

    public static Object jsonObtainObjFromString(String lstJson, Class clase) {
        return jsonObtainObjFromString(lstJson, clase, false);
    }
    
    public static Object jsonObtainObjFromString(String lstJson, Class clase, boolean isListObject) {        
        List<Object> lst = jsonObtainLstObjFromJsonArray(jsonObtainJsonArrayFromString(lstJson), clase);
        if (lst != null && !lst.isEmpty()) {            
            return isListObject ? lst : lst.get(0);
        }
        return null;
    }

    private static List<Object> jsonObtainLstObjFromJsonArray(JsonArray lstJson, Class clase) {        
        List<Object> lstReturn = null;
        if (lstJson != null) {
            lstReturn = new ArrayList<Object>();
            for (int i = 0; i < lstJson.size(); i++) {                
                lstReturn.add(new Gson().fromJson(lstJson.get(i), clase));
            }
        }
        return lstReturn;
    }

    private static JsonArray jsonObtainJsonArrayFromString(String jsonString) {
        try {
            if (jsonString != null && !jsonString.isEmpty()) {
                return (JsonArray) new JsonParser().parse(jsonString);
            } else {                
                return null;
            }
        } catch (com.google.gson.JsonSyntaxException se) {
            if (!jsonString.equals("Error: No existe nigún registro")) {
                se.printStackTrace();
                System.err.println("MSMException (obtenerArray): " + se.toString() + ", parametro: " + jsonString);
            } 
            return null;
        } catch (ClassCastException cce) {
            JsonArray array = new JsonArray();
            array.add(new JsonParser().parse(jsonString));
            return array;
        }
    }
    
}
