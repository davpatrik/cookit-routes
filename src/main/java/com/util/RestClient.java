package com.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.ws.rs.ProcessingException;

public class RestClient {

    public static Object get(String urlParam, boolean isListObject, Class objectClass) {
        Object responseObj = null;
        try {

            URL url = new URL(urlParam);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String line;
            String response = "";            
            while ((line = br.readLine()) != null) {
                response = response.concat(line);
            }
            //System.out.println("responseGet: " + response);
            
            responseObj = AppUtil.jsonObtainObjFromString(response, objectClass, isListObject);            

            conn.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseObj;
    }

    public static Object post(String urlParam, Object payload, boolean isListObject) {
        Object responseObj = null;
        try {

            URL url = new URL(urlParam);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            String input = AppUtil.jsonObtenerStrDesdeObjeto(payload);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());            
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String line;
            String response = "";            
            while ((line = br.readLine()) != null) {
                response = response.concat(line);
            }
            responseObj = AppUtil.jsonObtainObjFromString(response, Object.class, isListObject);
            System.out.println("responseObj_post: " + AppUtil.jsonObtenerStrDesdeObjeto(responseObj));

            conn.disconnect();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return responseObj;
    }

    /* 
    public static void main(String args[]) throws ProcessingException, InterruptedException, Exception {
        String urlCookIt = "https://cookit.proxy.beeceptor.com/items";
        String url = "http://localhost:8080/user/login";

        String strPayload = "{\n"
                + "    \"user\": \"asdsdf\",\n"
                + "    \"password\": \"pAsSwORd\"\n"
                + "}";
        Object payload = AppUtil.jsonObtainObjFromString(strPayload, Object.class, true);
        // payload = new UserDto("user111", "pas");
        //get(url, AppUtil.jsonObtainObjFromString(payload, Object.class, true));
        //get(urlCookIt, true);

        Object objX = post(url, payload, false);
        System.out.println("objX: " + AppUtil.jsonObtenerStrDesdeObjeto(objX));

        //Object postObj = generalRestClient_Post(url, payload, UserDto.class);
        //System.out.println("postObj: " + AppUtil.jsonObtenerStrDesdeObjeto(postObj));
    }
    */

}
