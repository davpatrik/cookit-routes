package com.cookit.service.impl;

import com.cookit.CookItRoutesApplication;
import com.cookit.dto.ItemDto;
import com.cookit.dto.ProteinsDto;
import com.util.RestClient;
import java.util.List;
import org.springframework.stereotype.Service;
import com.cookit.service.ItemsService;

/**
 *
 * @author davpatrik
 */
@Service
public class ItemsServiceImpl implements ItemsService {

    private final String DEFAULT_COOKIT_SERVICE_ENDPOINT = "https://cookit.proxy.beeceptor.com/";

    @Override
    public List<ItemDto> queryLstItems() {        
        String URL_PARAM = getUrl("items");        
        List<ItemDto> lstItemsQueried = (List<ItemDto>) RestClient.get(URL_PARAM, true, ItemDto.class);        
        return lstItemsQueried;
    }

    @Override
    public List<ProteinsDto> queryLstProteins() {
        String URL_PARAM = getUrl("proteins");        
        List<ProteinsDto> lstProteinsQueried = (List<ProteinsDto>) RestClient.get(URL_PARAM, true, ProteinsDto.class);        
        return lstProteinsQueried;
    }

    public String extractProductMealCode(ItemDto itemDto) {
        return itemDto.getDisplayName().split("-")[1];
    }

    private String getUrl(String endPoint) {
        String URL_PARAM = (CookItRoutesApplication.getEnv() != null
                ? CookItRoutesApplication.getEnv().getProperty("cookit.service.endpoint")
                : DEFAULT_COOKIT_SERVICE_ENDPOINT) + endPoint;
        return URL_PARAM;
    }

}
