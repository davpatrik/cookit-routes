package com.cookit.service.impl;

import com.cookit.dto.ItemDto;
import com.cookit.dto.ProteinsDto;
import com.cookit.dto.RoutesPayloadDto;
import com.cookit.dto.RoutesResponseDto;
import org.springframework.stereotype.Service;
import com.cookit.service.RoutesService;
import com.util.AppUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author dparedes
 */
@Service
public class RoutesServiceImpl implements RoutesService {

    @Autowired
    private ItemsServiceImpl productService;

    @Override
    public ResponseEntity processMessage(RoutesPayloadDto payload) {
        if (!isPayloadValid(payload)) {
            return new ResponseEntity<>(payload, HttpStatus.BAD_REQUEST);
        } else {
            RoutesResponseDto response = queryRoutes(payload);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    public RoutesResponseDto queryRoutes(RoutesPayloadDto payload) {        
        List<ItemDto> lstItemsQueried = null;
        List<ItemDto> lstItemsToProcess = null;
        List<ProteinsDto> lstProteins = null;        
        // Response
        RoutesResponseDto responseDto = new RoutesResponseDto(Arrays.asList(), Arrays.asList());
        List<String> lstPicks = new ArrayList<>();
        List<String> lstOutOfStock = new ArrayList<>();
        try {
            // Query items            
            lstItemsQueried = productService.queryLstItems();
            // Filter items to process
            lstItemsToProcess = lstItemsQueried.stream().filter(objX -> payload.getItemIds().contains(objX.getId())).collect(Collectors.toList());
            if (lstItemsToProcess != null && !lstItemsToProcess.isEmpty()) {                
                System.out.println("lstItemsToProcees: " + AppUtil.jsonObtenerStrDesdeObjeto(lstItemsToProcess));
                for (ItemDto itemX : lstItemsToProcess) {
                    // Validate stock (volume)
                    if(itemX.getVolume().equals(0)){
                        lstOutOfStock.add(itemX.getStation());
                    } else {
                        lstPicks.add(itemX.getStation());
                    }
                    // Extract poduct meal code
                    String mealCode = productService.extractProductMealCode(itemX);
                    System.out.println("mealCode: " + mealCode);
                    if(mealCode != null && !mealCode.isEmpty()){
                        // Query proteins
                        lstProteins = lstProteins == null ? productService.queryLstProteins() : lstProteins; // Query proteins once per iteraction
                        //System.out.println("lstProteins: " + AppUtil.jsonObtenerStrDesdeObjeto(lstProteins));
                        System.out.println("mealCodeX: " + mealCode);
                        List<ProteinsDto> lstProteinFiltered = lstProteins.stream().filter(protX -> protX.getCode().equals(mealCode)).collect(Collectors.toList());
                        System.out.println("lstProteinFiltered: " + AppUtil.jsonObtenerStrDesdeObjeto(lstProteinFiltered));
                        if(lstProteinFiltered != null && !lstProteinFiltered.isEmpty()){
                            lstPicks.add(lstProteinFiltered.get(0).getStation());
                        }
                    }
                }                                
                responseDto = new RoutesResponseDto(lstPicks, lstOutOfStock);
            }
        } catch (Exception ex) {
            Logger.getLogger(RoutesServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            lstItemsQueried = null;
            lstItemsToProcess = null;
            lstProteins = null;
        }
        return responseDto;
    }

    @Override
    public boolean isPayloadValid(RoutesPayloadDto payload) {
        return payload != null && payload.getItemIds() != null && !payload.getItemIds().isEmpty();
    }    
    
}
