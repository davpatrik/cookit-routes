package com.cookit.service;

import com.cookit.dto.ItemDto;
import com.cookit.dto.ProteinsDto;
import java.util.List;

/**
 *
 * @author dparedes
 */
public interface ItemsService {
    
    public List<ItemDto> queryLstItems();
    public List<ProteinsDto> queryLstProteins();
    
}
