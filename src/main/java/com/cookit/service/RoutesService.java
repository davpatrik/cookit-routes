package com.cookit.service;

import com.cookit.dto.RoutesPayloadDto;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author dparedes
 */
public interface RoutesService {
    
    public ResponseEntity processMessage(RoutesPayloadDto messageDto);
    public boolean isPayloadValid(RoutesPayloadDto payload);
    
}
