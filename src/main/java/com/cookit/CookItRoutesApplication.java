package com.cookit;

import com.cookit.security.JWTAuthorizationFilter;
import javax.ws.rs.HttpMethod;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author dparedes
 */
@SpringBootApplication
@ComponentScan("com.cookit")
public class CookItRoutesApplication {

    private static Environment env;    
    
    public static void main(String[] args) {
        //SpringApplication.run(CookItRoutesApplication.class, args);
        SpringApplication app = new SpringApplication(CookItRoutesApplication.class);
        env = app.run(args).getEnvironment();        
    }

    @EnableWebSecurity
    @Configuration
    class WebSecurityConfig extends WebSecurityConfigurerAdapter {

        //@Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                    .authorizeRequests()                    
                    .antMatchers(HttpMethod.GET, "/user/login").permitAll()                    
                    .anyRequest().authenticated();
            http.cors();
        }
    }

    public static Environment getEnv() {
        return env;
    }   
    
}
