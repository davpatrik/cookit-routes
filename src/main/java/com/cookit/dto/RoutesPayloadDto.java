package com.cookit.dto;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
public class RoutesPayloadDto {

    private List<Integer> itemIds;

    public RoutesPayloadDto(List<Integer> itemIds) {
        this.itemIds = itemIds;
    }

}
