package com.cookit.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author davpatrik
 */
@Getter
@Setter
@NoArgsConstructor
public class ItemDto {

    private Integer id;
    private String name;
    private String displayName;
    private Integer volume;
    private String deliveryWeek;
    private String station;
    private String category;

}
