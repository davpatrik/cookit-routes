package com.cookit.dto;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class RoutesResponseDto {

    private List<String> picks;
    private List<String> outOfStock;

    public RoutesResponseDto(List<String> picks, List<String> outOfStock) {
        this.picks = picks;
        this.outOfStock = outOfStock;
    }

}
