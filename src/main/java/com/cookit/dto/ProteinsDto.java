package com.cookit.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author davpatrik
 */
@Getter
@Setter
@NoArgsConstructor
public class ProteinsDto {
    
    private String name;
    private String code;
    private String station;

    public ProteinsDto(String name, String code, String station) {
        this.name = name;
        this.code = code;
        this.station = station;
    }
    
}
