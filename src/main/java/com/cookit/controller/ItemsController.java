package com.cookit.controller;

import com.cookit.dto.ItemDto;
import com.cookit.service.impl.ItemsServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dparedes
 */
@RestController
@RequestMapping("/items")
@CrossOrigin(origins = "*")
public class ItemsController {
       
    @Autowired    
    private ItemsServiceImpl itemsService;
    
    //@GetMapping("")
    @PostMapping("")
    public ResponseEntity queryLstItems() {
         List<ItemDto> lstRespone = itemsService.queryLstItems();
         return new ResponseEntity<>(lstRespone, HttpStatus.OK);
    }

}
