package com.cookit.controller;

import com.cookit.dto.RoutesPayloadDto;
import com.cookit.service.impl.RoutesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dparedes
 */
@RestController
@RequestMapping("/routes")
@CrossOrigin(origins = "*")
public class RoutesController {
       
    @Autowired    
    private RoutesServiceImpl routesService;
    
    //@GetMapping("")
    @PostMapping("")
    public ResponseEntity queryRoutesByItems(@RequestBody RoutesPayloadDto payload) {
        return routesService.processMessage(payload);        
    }

}
